//auto complete
function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /execute a function when someone writes in the text field:/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /close any already open lists of autocompleted values/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /create a DIV element that will contain the items (values):/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /append the DIV element as a child of the autocomplete container:/
        this.parentNode.appendChild(a);
        /for each item in the array.../
        for (i = 0; i < arr.length; i++) {
            /check if the item starts with the same letters as the text field value:/
            if ((arr[i].toUpperCase()).includes(val.toUpperCase())) {
                /create a DIV element for each matching element:/
                b = document.createElement("DIV");
                /make the matching letters bold:/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /insert a input field that will hold the current array item's value:/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /execute a function when someone clicks on the item value (DIV element):/
                b.addEventListener("click", function (e) {
                    /insert the value for the autocomplete text field:/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /execute a function presses a key on the keyboard:/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /and and make the current item more visible:/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /and and make the current item more visible:/
            addActive(x);
        } else if (e.keyCode == 13) {
            /If the ENTER key is pressed, prevent the form from being submitted,/
            e.preventDefault();
            if (currentFocus > -1) {
                /and simulate a click on the "active" item:/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /a function to classify an item as "active":/
        if (!x) return false;
        /start by removing the "active" class on all items:/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /add class "autocomplete-active":/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /a function to remove the "active" class from all autocomplete items:/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /execute a function when someone clicks in the document:/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}
var products = [
    "New Collection",
    "Best Sellers",
    "Barbie Cakes",
    "Fruit Cakes",
    "Personalized Cakes",
    "Fancy Cakes",
    "Tier Cakes",
    "Sugar Free Cakes",
    "Tyheme Cakes",
    "Eesigner Cakes",
    "Best Collections",
    "Kids Cakes",
    "Chocolate Cakes",
    "Emoji Cakes",
    "Fresh Designs",
    "Flower Arrangements",
    "Flower Bouquet",
    "Basekt Arrangments",
    "Vase Arrangements",
    "Garlands",
    "Chocolate Bouquets",
    "Scheme"
];

autocomplete(document.getElementById("search-items"), products);

var searchCities = [
    "Andhra Pradesh",
    "Arunachal Pradesh",
    "Assam",
    "Bihar",
    "Chattisgarh",
    "Goa",
    "Gujarat",
    "Haryana",
    "Himachal Pradesh",
    "Jharkhand",
    "Karnataka",
    "Kerala",
    "Madha Pradesh",
    "Maharastra",
    "Manipur",
    "Meghalaya",
    "Mizoram",
    "Nagaland",
    "Odisha",
    "Punjab",
    "Rajasthan",
    "Sikkim",
    "Tamilnadu",
    "Telangana",
    "Tripura",
    "Uttarakhand",
    "Utter Pradesh",
    "West Bengal"
];

autocomplete(document.getElementById("search-cities"), searchCities);





var swiper = new Swiper('.mainSlider', {
    autoplay: {
        delay: 5000,
        disableOnInteraction: true,
    },
    autoHeight: true, //enable auto height
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: false,
        clickable: true,
    },
});


//click event to move top
$(window).scroll(function () {
    // $("#movetop").hide();
    if ($(this).scrollTop() > 100) {
        $('#movetop').fadeIn();
    } else {
        $('#movetop').fadeOut();
    }
});

//click event to scroll top
$('#movetop').click(function () {
    $('html, body').animate({
        scrollTop: 0
    }, 'fast')
});

//on scroll add class to header 
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.fixed-top').addClass('fixed-theme');
    } else {
        $('.fixed-top').removeClass('fixed-theme');
    }
});

//on hover pproduct item 
// $(document).ready(function () {
//     $('.productItemCol').hover(function () {
//         $(this).addClass("hoverProductItem");
//     },
//         function () {
//             $(this).removeClass("hoverProductItem");
//         }
//     );
// });


//star rating
jQuery(document).ready(function ($) {

    $(".btnrating").on('click', (function (e) {

        var previous_value = $("#selected_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#selected_rating").val(selected_value);

        $(".selected-rating").empty();
        $(".selected-rating").html(selected_value);

        for (i = 1; i <= selected_value; ++i) {
            $("#rating-star-" + i).toggleClass('btn-warning');
            $("#rating-star-" + i).toggleClass('btn-default');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#rating-star-" + ix).toggleClass('btn-warning');
            $("#rating-star-" + ix).toggleClass('btn-default');
        }

    }));
});

document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'complete') {
        setTimeout(function () {
            document.getElementById('load').style.visibility = "hidden";
        }, 1000);
    }
}


//product detail gallery
var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});
var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs
    }
});


$('.show').zoomImage();
$('.show-small-img:first-of-type').css({ 'border': 'solid 1px #951b25', 'padding': '2px' })
$('.show-small-img:first-of-type').attr('alt', 'now').siblings().removeAttr('alt')
$('.show-small-img').click(function () {
    $('#show-img').attr('src', $(this).attr('src'))
    $('#big-img').attr('src', $(this).attr('src'))
    $(this).attr('alt', 'now').siblings().removeAttr('alt')
    $(this).css({ 'border': 'solid 1px #951b25', 'padding': '2px' }).siblings().css({ 'border': 'none', 'padding': '0' })
    if ($('#small-img-roll').children().length > 4) {
        if ($(this).index() >= 3 && $(this).index() < $('#small-img-roll').children().length - 1) {
            $('#small-img-roll').css('left', -($(this).index() - 2) * 76 + 'px')
        } else if ($(this).index() == $('#small-img-roll').children().length - 1) {
            $('#small-img-roll').css('left', -($('#small-img-roll').children().length - 4) * 76 + 'px')
        } else {
            $('#small-img-roll').css('left', '0')
        }
    }
})
$('#next-img').click(function () {
    $('#show-img').attr('src', $(".show-small-img[alt='now']").next().attr('src'))
    $('#big-img').attr('src', $(".show-small-img[alt='now']").next().attr('src'))
    $(".show-small-img[alt='now']").next().css({ 'border': 'solid 1px #951b25', 'padding': '2px' }).siblings().css({ 'border': 'none', 'padding': '0' })
    $(".show-small-img[alt='now']").next().attr('alt', 'now').siblings().removeAttr('alt')
    if ($('#small-img-roll').children().length > 4) {
        if ($(".show-small-img[alt='now']").index() >= 3 && $(".show-small-img[alt='now']").index() < $('#small-img-roll').children().length - 1) {
            $('#small-img-roll').css('left', -($(".show-small-img[alt='now']").index() - 2) * 76 + 'px')
        } else if ($(".show-small-img[alt='now']").index() == $('#small-img-roll').children().length - 1) {
            $('#small-img-roll').css('left', -($('#small-img-roll').children().length - 4) * 76 + 'px')
        } else {
            $('#small-img-roll').css('left', '0')
        }
    }
})

$('#prev-img').click(function () {
    $('#show-img').attr('src', $(".show-small-img[alt='now']").prev().attr('src'))
    $('#big-img').attr('src', $(".show-small-img[alt='now']").prev().attr('src'))
    $(".show-small-img[alt='now']").prev().css({ 'border': 'solid 1px #951b25', 'padding': '2px' }).siblings().css({ 'border': 'none', 'padding': '0' })
    $(".show-small-img[alt='now']").prev().attr('alt', 'now').siblings().removeAttr('alt')
    if ($('#small-img-roll').children().length > 4) {
        if ($(".show-small-img[alt='now']").index() >= 3 && $(".show-small-img[alt='now']").index() < $('#small-img-roll').children().length - 1) {
            $('#small-img-roll').css('left', -($(".show-small-img[alt='now']").index() - 2) * 76 + 'px')
        } else if ($(".show-small-img[alt='now']").index() == $('#small-img-roll').children().length - 1) {
            $('#small-img-roll').css('left', -($('#small-img-roll').children().length - 4) * 76 + 'px')
        } else {
            $('#small-img-roll').css('left', '0')
        }
    }
})
